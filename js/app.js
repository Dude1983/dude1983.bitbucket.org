config = {
  "package": [{
      "name": "QUAD - The Event Essentials",
          "description": "eMarketing, eReg, eHome, eSurvey",
          "prices": [{
          "lessThan": 1001,
              "perRegPrice": 5.00
      }, {
          "lessThan": 2000,
              "perRegPrice": 4.75
      }, {
          "lessThan": 3000,
              "perRegPrice": 4.50
      }, {
          "lessThan": 4000,
              "perRegPrice": 4.00
      }, {
          "lessThan": 5000,
              "perRegPrice": 3.75
      }, {
          "lessThan": 10000,
              "perRegPrice": 3.50
      }, {
          "lessThan": 20000,
              "perRegPrice": 3.25
      }, {
          "lessThan": 30000,
              "perRegPrice": 3.00
      }, {
          "lessThan": 40000,
              "perRegPrice": 2.75
      }, {
          "lessThan": 50000,
              "perRegPrice": 2.50
      }]
  }, {
      "name": "PRO - The Event Power Suite",
          "description": "eMarketing, eReg, eHome, eSurvey, eSeating",
          "image": "",
          "prices": [{
          "lessThan": 1001,
              "perRegPrice": 6.00
      }, {
          "lessThan": 2000,
              "perRegPrice": 5.75
      }, {
          "lessThan": 3000,
              "perRegPrice": 5.35
      }, {
          "lessThan": 4000,
              "perRegPrice": 5.00
      }, {
          "lessThan": 5000,
              "perRegPrice": 4.75
      }, {
          "lessThan": 10000,
              "perRegPrice": 4.50
      }, {
          "lessThan": 20000,
              "perRegPrice": 4.25
      }, {
          "lessThan": 30000,
              "perRegPrice": 4.00
      }, {
          "lessThan": 40000,
              "perRegPrice": 3.75
      }, {
          "lessThan": 50000,
              "perRegPrice": 3.50
      }]
  }],

  "term": [{
      'name': '12 months',
          'value': 12,
          'factor': 2
  }, {        
      'name': '24 months',
          'value': 24,
          'factor': 1
  }, {
      'name': '36 months',
          'value': 36,
          'factor': 0.95
  }, {
      'name': '48 months',
          'value': 48,
          'factor': 0.90
  }, {
      'name': '60 months',
          'value': 60,
          'factor': 0.85
  }],

  "currency": [{
      'name': '€ EUR',
          'value': 'EUR',
          'symbol': '€',
          'factor': 0.8
  }, {
      'name': '£ GBP',
          'value': 'GBP',
          'symbol': '£',
          'factor': 0.66
  }, {
      'name': '$ USD',
          'value': 'USD',
          'symbol': '$',
          'factor': 1
  }]
};

eSelect = {
  "setupFee": 250,
  "prices": [{
      "lessThan": 101,
      "perSubmissionPrice": 4.00
      }, {
      "lessThan": 501,
      "perSubmissionPrice": 3.75 
      }, {
      "lessThan": 1001,
      "perSubmissionPrice": 3.50
      }] 
};      



$(function () {

  //populate the dropdowns
  $('form select').each(function () {
      var select = this;
      $.each(config[select.id], function () {
          $(select).append($("<option />").val(this.value || this.name).text(this.name));
      });
  });

  var calculate = function () {

      var getSelectedItem = function (items, selected) {
          return items.filter(function (item) {
              return (item.value || item.name) == selected;
          })[0];
      };

      var getSelections = function () {
          var result = {};
          $('form select, form input').each(function () {
              var value = $(this).val();
              var key = $(this).attr('id');
              result[key] = value;

              var parents = $(this).parents('div.form-group');

              if ($(this).hasClass('required') && !value) {
                  parents.addClass('has-error');
              } else {
                  parents.removeClass('has-error');

                  if (key in config) {
                      result[key] = getSelectedItem(config[key], value);
                  }
              }
          });
          return result;
      };

      var selections = getSelections();

      if ($('form div.has-error').length) {
          return $('div.result span').text('N/A');
      }

      Number.prototype.printCurrency = function () {
          return !this ? 'N/A' : selections.currency.symbol + this.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
      };
      

      var price;
      var submissionPrice;

      $.each(selections.package.prices, function () {
          price = this.perRegPrice;
          if (selections.registrations < this.lessThan) {
              return false;
          }
      });  

      $.each(eSelect.prices, function () {
          submissionPrice = this.perSubmissionPrice;
          if (selections.submissions < this.lessThan) {
              return false;
          }
      }); 

  
      var perRegPrice = price * selections.currency.factor * selections.term.factor;
      var perSubmissionPrice = submissionPrice * selections.currency.factor;

      var totalRegCost = perRegPrice * selections.registrations;
      var totalSubCost = perSubmissionPrice * selections.submissions;
      var numberOfYears = selections.term.value / 12;


      $('#per-reg-price').text(perRegPrice.printCurrency());
      $('#per-submission-price').text(perSubmissionPrice.printCurrency());
 
      $('#acv').text((totalRegCost + totalSubCost).printCurrency());
      $('#tcv').text(((totalRegCost + totalSubCost) * numberOfYears).printCurrency());

  };

   $('.eSelect').click(function() {
    $('#submissionForm, #perSubmissionPrice').toggle( 'fast', function() {
      $('#per-submission-price').text('N/A');
    });
  });

  $('select').change(calculate);
  $('input[type="number"]').keyup(calculate);
});